#!/bin/sh
#
# Date  	Version Author          		Comments
# 19/12/11      1.1     JvdO Transfer-solutions         Initial Version
#
#
# Description
# ASM backup methodiek with md_backup
# Should be run as the grid user
# ##############################################

# Tunables
# ##############################################

BACKUPDIR=/hab-data1/rac-backup/asmmd
ERR=0

# ##############################################

# ##############################################
# Start of functions
#
# asm_metadata_full - backup the asm metadata
# asm_metadata_dg   - backup the asm metadata by DG
# ##############################################

asm_metadata_full()
{

export ORACLE_SID=`cat /etc/oratab | grep ^+ASM | awk 'BEGIN {FS=":"}; {print $1}'  `
export ORACLE_HOME=`cat /etc/oratab | grep ^+ASM | awk 'BEGIN {FS=":"}; {print $2}'  `

echo "INFO: Backup of full ASM started at `date`"
if [ -f ${BACKUPDIR}/asm_metadata_full.bkp ]
   then
      echo "INFO: Moving the file ${BACKUPDIR}/asm_metadata_full.bkp to ${BACKUPDIR}/asm_metadata_full.old"
      cp ${BACKUPDIR}/asm_metadata_full.bkp ${BACKUPDIR}/asm_metadata_full.old 2>/dev/null
      if [ $? -ne 0 ]
         then
            echo "WARN: Unable to backup ${BACKUPDIR}/asm_metadata_${DG}.bkp, will overwrite it anyway"
      fi
            rm -f ${BACKUPDIR}/asm_metadata_full.bkp 2>/dev/null
fi

echo "INFO: Backing up the ASM to ${BACKUPDIR}/asm_metadata_full.bkp"
rm ${BACKUPDIR}/asm_metadata_full.err 2>/dev/null
${ORACLE_HOME}/bin/asmcmd md_backup ${BACKUPDIR}/asm_metadata_full.bkp 2>${BACKUPDIR}/asm_metadata_full.err

if [ -s ${BACKUPDIR}/asm_metadata_full.err ]
   then
       echo ""
       echo "WARN: Unable to backup the ASM to ${BACKUPDIR}/asm_metadata_full.bkp"
       cat ${BACKUPDIR}/asm_metadata_full.err
       echo ""
       ERR=98
    else
       echo "INFO: Backup of ASM completed at `date`"
fi
}

# ##############################################
# End of functions
# ##############################################

### Main Program

echo "INFO: ASM Metadata backup started at `date`"
echo ""
echo "INFO: This script is being run as user `/usr/bin/id -un`"
echo ""

# create the backup directory if it does not exist
# ##############################################

if [ ! -d ${BACKUPDIR} ]
then
        mkdir ${BACKUPDIR} 2>/dev/null
        echo "INFO: Created the directory ${BACKUPDIR} as it did not exist"
        echo ""
                if [ $? -ne 0 ]
                then
                        echo "ERROR: Unable to create the directory ${BACKUPDIR}"
                        exit 99
                fi
fi

# backup the asm metadata
#
# FULL ASM backup
# ##############################################
asm_metadata_full

# Finish
# ##############################################

if [ ${ERR} != 0 ]
then
        echo "ERROR: An error occurred backing up one or more disk groups, see above"
        exit 98
else
        echo "INFO: ASM Metadata backup completed at `date`"
fi
