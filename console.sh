#!/bin/ksh
#
#===================================================================
# EM dbconsole start/stop/status opvragen voor beide databases
#
# Author: Lau Daniels 17-04-2007
# Modified:  Lau Daniels 09-05-2007 / 16-02-2012
#
#===================================================================

#===================================================================
#
# do_emctl
# depending on parameter -- startup, shutdown, status
#
#====================================================================

do_emctl() {

echo "DBCONSOLE $1 for oracle_sid = $ORACLE_SID"
case "$1" in
    start)
	#start dbconsole incl. agent
	$ORACLE_HOME/bin/emctl start dbconsole
	;;

    stop)
	#stop dbconsole incl. agent
	$ORACLE_HOME/bin/emctl stop dbconsole
	;;

    status)
	#status dbconsole incl. agent
	$ORACLE_HOME/bin/emctl status dbconsole
	$ORACLE_HOME/bin/emctl status agent
	;;

    *)
	"Usage: $0 start|stop|status applies to both databases"
	exit 1
esac
}

#=====================================================================
#
# Main procedure
# export de SIDS en do $ORACLE_HOME/bin/emctl stuff
#
#=====================================================================


	export ORACLE_SID=DATABASE-RAC
	do_emctl $1

exit 0

